# Pico Wizard

A Post Installation Configuration Wizard for Linux

## Overview

Pico Wizard is a setup wizard meant for configuring pre-installed oses like linux phones, raspberry pi and other embedded systems.
Currently PicoWizard is being used by ManjaroARM Plasma Mobile Image for PinePhone.
